readme

Bash script to automatically compile latex script (call latex, bibtex, dvips and ps2pdf : generate final pdf from tex file) on Unix and Mac OS X

Usage:
./compile_latex.sh paper_version12.tex
 where the name of the tex file is paper_version12.tex

Assumes that latex, dvips and ps2pdf are already installed.